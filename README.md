# wiznet_w7500p

Module using the Wiznet W7500P microcontroller. Also two small debug headers, one with an additional security chip (ATSHA204A).

This design is released under the CERN OHL version 2.0. A copy of the text can be found here:

[] (https://ohwr.org/cern_ohl_p_v2.txt)
