#!/bin/env python

import socket, crcmod

class interface():

        MAX_BLOCK_SIZE = 2048

        # Wiznet commands & responses
        SPI_SEND_RECEIVE_COMMAND = 55
        SPI_SEND_RECEIVE_RESPONSE = 1
        
        FLASH_WRITE_COMMAND = 4
        FLASH_WRITE_RESPONSE = 5
        FLASH_READ_COMMAND = 6
        FLASH_READ_RESPONSE = 7

        I2C_COMMAND = 8
        I2C_RESPONSE = 9

        ATSHA_WAKE_COMMAND = 10
        ATSHA_WAKE_RESPONSE = 11

        ATSHA_SLEEP_COMMAND = 12
        ATSHA_SLEEP_RESPONSE = 13

        ATSHA_READ_CFG_COMMAND = 14
        ATSHA_READ_CFG_RESPONSE = 15

        ATSHA_RANDOM_COMMAND = 16
        ATSHA_RANDOM_RESPONSE = 17

        GPIO_COMMAND = 18
        GPIO_RESPONSE = 19

        GPIO_MODE_GET_INPUT_VALUE = 0
        GPIO_MODE_SET_OUTPUT_VALUE = 1
        GPIO_MODE_OUT_nIN = 2
        GPIO_MODE_PULL_DISABLE = 3
        GPIO_MODE_PULL_UP_nDOWN = 4

        # Wiznet error codes
        WIZNET_ERROR_CODES = {
          252 : 'Timeout',
          253 : 'SPI CRC mismatch',
          254 : 'Insufficient input data',
          255 : 'Unknown command',
        }

        # Wiznet error codes
        SPI_SLAVE_ERROR_CODES = {
          1 : 'Bad write register address',
          2 : 'Bad read register address',
        }

        def __init__(self, *, target):
            # Interface socket
            self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print('a')
            self.__socket.connect((target, int(50010)))
            print('b')

            # Send / receiver buffers
            # Note we could add overflow exceptions
            self.__responses = []
            self.__commands = []
            self.__commandBuffer = bytearray()

            # Count the number of devices
            self.__numDevices = self.__SPIDeviceCount()

        def numDevices(self):
            return self.__numDevices
        
        def __SPIDeviceCount(self):
            # This block triggers an SPI read of device 0, register 0
            # It's a benign read-back that will tell us how many devices are in the chain
            # as each device subtracts one from the device address
            sendmsg = bytearray([self.SPI_SEND_RECEIVE_COMMAND, 0x80, 0, 0, 0])

            # Send the commands and flush the data
            self.__socket.send(sendmsg)

            # TODO: timeout
            res = bytearray()
            count = 0
            try:
                data2 = self.__socket.recv(self.MAX_BLOCK_SIZE)
                if not data2:
                    raise Exception('No data received')
            except KeyboardInterrupt:
                print('Ctrl-C detected')
                exit()
            count = count + len(data2)
            res = res + bytearray(data2)
            # An SPI read response is seven bytes long
            #if count == 7:
            #    break

            # Treat timeout error as 0 devices
            if res[0] == 252:
                return 0
            if res[0] > 127:
                raise Exception('Error code from Wiznet ({:d} - {:s})'.format(res[0], self.WIZNET_ERROR_CODES.get(res[0], 'Unrecognized')))
            if res[0] != self.SPI_SEND_RECEIVE_RESPONSE:
                raise Exception('Did not receive normal SPI response from Wiznet {:d}'.format(res[0]))
            res = res[1] & 0x3F
            return 64-res

        def atshaWake(self):
            if (len(self.__commandBuffer) + 1) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.ATSHA_WAKE_COMMAND)
            self.__commands.append([self.ATSHA_WAKE_COMMAND])
            self.doPending()

        def atshaSleep(self):
            if (len(self.__commandBuffer) + 1) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.ATSHA_SLEEP_COMMAND)
            self.__commands.append([self.ATSHA_SLEEP_COMMAND])
            self.doPending()

        def atshaRandom(self):
            if (len(self.__commandBuffer) + 1) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.ATSHA_RANDOM_COMMAND)
            self.__commands.append([self.ATSHA_RANDOM_COMMAND])
            self.doPending()
            return self.__getResponse()[3]
        
        def atshaReadCfg(self, registerAddress, readCount):
            if (len(self.__commandBuffer) + 3) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.ATSHA_READ_CFG_COMMAND)
            self.__commandBuffer.append(registerAddress)
            self.__commandBuffer.append(readCount)
            self.__commands.append([self.ATSHA_READ_CFG_COMMAND, registerAddress, readCount])
            self.doPending()
            return self.__getResponse()[3]

        def setNetworkSettings(self, settings):
            data = bytearray(settings['MAC'])
            data += bytearray(settings['IP'])
            data += bytearray(settings['GATEWAY'])
            data += bytearray(settings['SUBNET'])
            if len(data) != 18:
                raise Exception('Requested network settings are incorrect size')
            data += bytearray(46)
            crc_function = crcmod.mkCrcFun(poly=0x18005, initCrc=0xFFFF, rev=True, xorOut=0)
            crc = crc_function(data)
            data += bytearray([(crc >> 8) & 0xFF, crc & 0xFF])
            self.__addFlashWriteCommand(data)
            self.__addFlashReadCommand(66)
            self.doPending()
            if self.__getResponse()[1] != data:
                raise Exception('Failed to store network address in flash')

        def getNetworkSettings(self):
            self.__addFlashReadCommand(66)
            self.doPending()
            res = self.__getResponse()[1]
            #for i in res:
            #    print(hex(i))
            crc_function = crcmod.mkCrcFun(poly=0x18005, initCrc=0xFFFF, rev=True, xorOut=0)
            crc = (int(res[64]) << 8) | res[65]
            if crc_function(res[:-2]) != crc:
                return
            mac = list(res[0:6])
            ip = list(res[6:10])
            gateway = list(res[10:14])
            subnet = list(res[14:18])
            return {
                'MAC' : mac,
                'IP' : ip,
                'GATEWAY' : gateway,
                'SUBNET' : subnet,
            }

        def addGPIOCommand(self, mode, pin, value):
            if (mode < 0) or (mode > 5):
                raise Exception('Invalid mode')
            if (pin < 0) or (pin > 14):
                raise Exception('Invalid pin')
            if (value != 0) and (value != 1):
                raise Exception('Invalid value')
            if (len(self.__commandBuffer) + 2) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.GPIO_COMMAND)
            self.__commandBuffer.append((mode << 5) | (pin << 1) | value)
            self.__commands.append([self.GPIO_COMMAND, (mode << 5) | (pin << 1) | value])

        def getGPIOReadResponse(self):
            return self.__getResponse()[1] & 1
        
        def addI2CCommand(self, channel, slaveAddress, writeBytes, readByteCount):
            # COMMAND
            # CHANNEL
            # SLAVE ADDRESS
            # WRITE BYTE COUNT
            # READ BYTE COUNT
            # [WRITE DATA 8b x WRITE BYTE COUNT]
            size = 5 + len(writeBytes) # Minimum size
            if (len(self.__commandBuffer) + size) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.I2C_COMMAND)
            self.__commandBuffer.append(channel)
            self.__commandBuffer.append(slaveAddress)
            self.__commandBuffer.append(len(writeBytes))
            self.__commandBuffer.append(readByteCount)
            self.__commandBuffer += writeBytes
            self.__commands.append([self.I2C_COMMAND, channel, slaveAddress, len(writeBytes), readByteCount])

        def getI2CReadResponse(self):
            return self.__getResponse()

        def __addFlashReadCommand(self, size):
            if size <= 0:
                raise Exception('Size cannot be negative or zero')
            if (len(self.__commandBuffer) + 2) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.FLASH_READ_COMMAND)
            self.__commandBuffer.append(size-1)
            self.__commands.append([self.FLASH_READ_COMMAND, size])
    
        def __addFlashWriteCommand(self, data):
            if len(data) == 0:
                raise Exception('Data buffer is empty')
            if (len(self.__commandBuffer) + 2 + len(data)) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.FLASH_WRITE_COMMAND)
            self.__commandBuffer.append(len(data)-1)
            for i in data:
                self.__commandBuffer.append(i)
            self.__commands.append([self.FLASH_WRITE_COMMAND, len(data)])

        def __addSPICommand(self, value1, value2):
            if (len(self.__commandBuffer) + 5) > self.MAX_BLOCK_SIZE:
                raise Exception('You overfilled the send buffer')
            self.__commandBuffer.append(self.SPI_SEND_RECEIVE_COMMAND)
            self.__commandBuffer.append((value1 >> 8) & 0xFF)
            self.__commandBuffer.append(value1 & 0xFF) 
            self.__commandBuffer.append((value2 >> 8) & 0xFF)
            self.__commandBuffer.append(value2 & 0xFF)
            self.__commands.append([self.SPI_SEND_RECEIVE_COMMAND, value1, value2])
        
        def __getResponse(self):
            if len(self.__responses) == 0:
                 return None
            res = self.__responses[0]
            self.__responses = self.__responses[1:]
            return res

        def __parseATSHAWakeResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            resp = []
            resp.append(self.ATSHA_WAKE_COMMAND)
            resp.append(data[1])
            self.__responses.append(resp)
            return 2

        def __parseATSHASleepResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            resp = []
            resp.append(self.ATSHA_SLEEP_COMMAND)
            resp.append(data[1])
            self.__responses.append(resp)
            return 2

        def __parseATSHAReadCfgResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            resp = []
            resp.append(self.ATSHA_READ_CFG_COMMAND)
            resp.append(data[1])
            if data[1] != 0:
                return 2
            # Don't parse full block unless complete
            if len(data) < data[2]+3:
                return 0
            resp.append(data[2])
            resp.append(data[3:3+data[2]])
            self.__responses.append(resp)
            return 3+data[2]
        
        def __parseATSHARandomResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            resp = []
            resp.append(self.ATSHA_RANDOM_COMMAND)
            resp.append(data[1])
            if data[1] != 0:
                return 2
            # Don't parse full block unless complete
            if len(data) < data[2]+3:
                return 0
            resp.append(data[2])
            resp.append(data[3:3+data[2]])
            self.__responses.append(resp)
            return 3+data[2]
        
        def __parseFlashReadResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            size = data[1] + 1
            if len(data) < size + 2:
                return 0
            resp = []
            resp.append(self.FLASH_READ_COMMAND)
            resp.append(data[2:data[1]+3])
            self.__responses.append(resp)
            return size + 2

        def __parseFlashReadResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            size = data[1] + 1
            if len(data) < size + 2:
                return 0
            resp = []
            resp.append(self.FLASH_READ_COMMAND)
            resp.append(data[2:data[1]+3])
            self.__responses.append(resp)
            return size + 2
        
        def __parseGPIOResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            resp = []
            resp.append(self.GPIO_COMMAND)
            resp.append(data[1])
            self.__responses.append(resp)
            return 2
        
        def __parseFlashWriteResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 2:
                return 0
            self.__responses.append([
                self.FLASH_WRITE_COMMAND,
                data[1]+1,
                ])
            return 2
        
        def __parseSPISendReceiveResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 7:
                return 0
            self.__responses.append([
                self.SPI_SEND_RECEIVE_COMMAND,
                (int(data[1] & 0xFF) << 8) | data[2],
                (int(data[3] & 0xFF) << 8) | data[4],
                (int(data[5] & 0xFF) << 8) | data[6],
                ])
            return 7
        
        def __parseI2CResponse(self, data):
            # Don't process unless we have enough data
            if len(data) < 6: # Need 6 minimum
                return 0
            if data[1] != 0:
                resp = [self.I2C_COMMAND]
                resp += data[1:6]
                self.__responses.append(resp)
                return 6
            totalReadCount = data[5] + 6 # Full message includes any read bytes
            if len(data) < totalReadCount:
                return 0
            resp = [self.I2C_COMMAND]
            resp += data[1:totalReadCount]
            self.__responses.append(resp)
            return totalReadCount

        def __parseReceived(self, data):
            if data[0] > 127:
                raise Exception('Error code from Wiznet ({:d} - {:s})'.format(data[0], self.WIZNET_ERROR_CODES.get(data[0], 'Unrecognized')))
            if data[0] == self.SPI_SEND_RECEIVE_RESPONSE:
                return self.__parseSPISendReceiveResponse(data)
            if data[0] == self.FLASH_READ_RESPONSE:
                return self.__parseFlashReadResponse(data)
            if data[0] == self.FLASH_WRITE_RESPONSE:
                return self.__parseFlashWriteResponse(data)
            if data[0] == self.ATSHA_WAKE_RESPONSE:
                return self.__parseATSHAWakeResponse(data)
            if data[0] == self.ATSHA_SLEEP_RESPONSE:
                return self.__parseATSHASleepResponse(data)
            if data[0] == self.ATSHA_READ_CFG_RESPONSE:
                return self.__parseATSHAReadCfgResponse(data)
            if data[0] == self.ATSHA_RANDOM_RESPONSE:
                return self.__parseATSHARandomResponse(data)
            if data[0] == self.GPIO_RESPONSE:
                return self.__parseGPIOResponse(data)
            if data[0] == self.I2C_RESPONSE:
                return self.__parseI2CResponse(data)
            raise Exception('Unknown response from Wiznet')

        def __validateSPISendReceiveResponse(self, cmd, rsp):
            [c0, c1, c2] = cmd
            [r0, r1, r2, r3] = rsp
            #print([hex(r1), hex(r2), hex(r3)])
            c1_device = (c1 >> 8) & 0x3F
            r1_device = ((r1 >> 8) & 0x3F) - 63 + self.__numDevices - 1
            c1_read = False if (c1 & 0x8000) == 0 else True
            r1_read = False if (r1 & 0x8000) == 0 else True
            c1 = (c1 & 0xC0FF) | c1_device
            r1 = (r1 & 0xC0FF) | r1_device
            #print(c1_device, r1_device)
            if r1 & 0x4000:
                    raise Exception('Received error response from device {:d}, error {:d} - {:s}'.format(r1_device, r2, self.SPI_SLAVE_ERROR_CODES.get(r2, 'Unrecognized')))
            if not(r1_read):
                if (r1 != c1) or (r2 != c2):
                    raise Exception('Command/response mismatch for SPI register write')
            else:
                if (r1 != c1):
                    raise Exception('Command/response mismatch for SPI register read')
                return rsp

                ####
                #crc_function = crcmod.mkCrcFun(poly=0x18005, initCrc=0xFFFF, rev=True, xorOut=0)
                #print(hex(crc_function(bytearray([c1 >> 8, c1 & 0xFF, c2 >> 8, c2 & 0xFF]))))
                #print([hex(c1), hex(c2)])
                #print([hex(r1), hex(r2), hex(r3)])
                #self.__commands.pop()
                #self.__responses.pop()
                ####

                #print(self.__commands[i])
                #print(self.__responses[i])

        def __validateFlashReadResponse(self, cmd, rsp):
            [c0, c1] = cmd
            [r0, r1] = rsp
            if (len(r1) != c1):
                raise Exception('Length mismatch for flash read')
            return rsp
        
        def __validateFlashWriteResponse(self, cmd, rsp):
            [c0, c1] = cmd
            [r0, r1] = rsp
            if (r1 != c1):
                raise Exception('Length mismatch for flash write')
            
        def __validateATSHAWakeResponse(self, cmd, rsp):
            [c0] = cmd
            [r0, r1] = rsp
            if (r1 != 0):
                raise Exception('Error {:d} during ATSHA204A wakeup'.format(r1))

        def __validateATSHASleepResponse(self, cmd, rsp):
            [c0] = cmd
            [r0, r1] = rsp
            if (r1 != 0):
                raise Exception('Error {:d} during ATSHA204A sleep'.format(r1))
            
        def __validateATSHAReadCfgResponse(self, cmd, rsp):
            [c0, c1, c2] = cmd
            if rsp[1] != 0:
                raise Exception('Error code {:d}'.format(rsp[1]))
            if rsp[2] != c2:
                raise Exception('Response size does not match expected size')
            return rsp
            
        def __validateATSHARandomResponse(self, cmd, rsp):
            [c0] = cmd
            if rsp[1] != 0:
                raise Exception('Error code {:d}'.format(rsp[1]))
            if rsp[2] != 32:
                raise Exception('Response size does not match expected size')
            return rsp
                    
        def __validateGPIOResponse(self, cmd, rsp):
            [c0, c1] = cmd
            [r0, r1] = rsp
            if (c1 & 0xFE) != (r1 & 0xFE):
                raise Exception('GPIO mode / pin mismatch between command and response')
            if ((c1 >> 5) & 0x7) != self.GPIO_MODE_GET_INPUT_VALUE:
                if (c1 & 1) != (r1 & 1):
                    raise Exception('GPIO value mismatch between command and response')
            else:
                return rsp

        def __validateI2CResponse(self, cmd, rsp):
            if rsp[1] != 0:
                raise Exception('Error {:d} during I2C transaction'.format(rsp[1]))
            if cmd[1] != rsp[2]:
                raise Exception('I2C channel mismatch between command and response')
            if cmd[2] != rsp[3]:
                raise Exception('I2C slave address')
            if cmd[3] != rsp[4]:
                raise Exception('I2C write byte count mismatch between command and response')
            if cmd[4] != rsp[5]:
                raise Exception('I2C read byte count mismatch between command and response')
            if cmd[4] != 0:
                return rsp[6:]

        def __validateCommandsAndResponses(self):
            reads = []
            for cmd, rsp in zip(self.__commands, self.__responses):
                if cmd[0] != rsp[0]:
                    raise Exception('Command/response mismatch in Wiznet communication')
                res = None
                if cmd[0] == self.SPI_SEND_RECEIVE_COMMAND:
                    res = self.__validateSPISendReceiveResponse(cmd, rsp)
                if cmd[0] == self.FLASH_READ_COMMAND:
                    res = self.__validateFlashReadResponse(cmd, rsp)
                if cmd[0] == self.FLASH_WRITE_COMMAND:
                    res = self.__validateFlashWriteResponse(cmd, rsp)
                if cmd[0] == self.ATSHA_WAKE_COMMAND:
                    res = self.__validateATSHAWakeResponse(cmd, rsp)
                if cmd[0] == self.ATSHA_SLEEP_COMMAND:
                    res = self.__validateATSHASleepResponse(cmd, rsp)
                if cmd[0] == self.ATSHA_READ_CFG_COMMAND:
                    res = self.__validateATSHAReadCfgResponse(cmd, rsp)
                if cmd[0] == self.ATSHA_RANDOM_COMMAND:
                    res = self.__validateATSHARandomResponse(cmd, rsp)
                if cmd[0] == self.GPIO_COMMAND:
                    res = self.__validateGPIOResponse(cmd, rsp)
                if cmd[0] == self.I2C_COMMAND:
                    res = self.__validateI2CResponse(cmd, rsp)
                if res != None:
                    reads.append(res)
            # At this point we flush the read commands as the write commands should have all been removed
            self.__commands = []
            # Replace the responses with just the validated reads
            self.__responses = reads

        def doPending(self):
            if len(self.__responses) != 0:
                raise Exception('You need to collect the data from the last call before calling doPending again')
            # Nothing to send
            if len(self.__commands) == 0:
                return
            
            #print('a')

            # Send the commands and flush the data
            self.__socket.send(self.__commandBuffer)
            self.__commandBuffer = bytearray()

            #print('b')
            
            # TODO: Add a timeout
            res = bytearray()
            #total = len(self.__commands) * 7
            #count = 0
            #print(total-count)
            while len(self.__commands) != len(self.__responses):
                try:
                    data2 = self.__socket.recv(self.MAX_BLOCK_SIZE)
                    if not data2:
                        raise Exception('No data received')
                except KeyboardInterrupt:
                    print('Ctrl-C detected')
                    exit()
                #count = count + len(data2)
                res = res + bytearray(data2)
                while len(res) != 0:
                    n = self.__parseReceived(res)
                    if n == 0:
                        break
                    res = res[n:]

            #print('c')

            self.__validateCommandsAndResponses()

            #print('d')

            # Check if we can read anything before trying
            #readable_sockets, _, _ = select.select([self.__socket], [], [], 0)
            #if len(readable_sockets) != 0:
            #    try:
            #        self.__receiveBuffer = self.__socket.recv(self.MAX_BLOCK_SIZE)
            #        if not self.__receiveBuffer:
            #            raise Exception('No data received')
            #    except KeyboardInterrupt:
            #        print('Ctrl-C detected')
            #        exit()

            #    print(self.__receiveBuffer)
            #    if self.__receiveBuffer[0] > 127:
            #         raise Exception('Error code detected from Wiznet ({:d})'.format(self.__receiveBuffer[0]))
            #    if len(self.__receiveBuffer) < 2:
            #         raise Exception('Under-minimum-length response from Wiznet')
            #    if self.__receiveBuffer[0] != 1:
            #         raise Exception('Did not receive expected response from Wiznet')
            #    if self.__receiveBuffer[1] != 0:
            #         raise Exception('SPI receiver overflow detected by Wiznet')
            #    if len(self.__receiveBuffer) & 1 == 1:
            #         #print(self.__receiveBuffer)
            #         #time.sleep(1)
            #         #self.__receiveBuffer = self.__socket.recv(self.MAX_BLOCK_SIZE)
            #         #print(self.__receiveBuffer)
            #         raise Exception('Received data volume wasn\'t a multiple of two')
            #    # Mark available data if there's more than two bytes in the response
            #    if len(self.__receiveBuffer) > 2:
            #        self.__receiveBufferPtr = 2
            #    else:
            #        self.__receiveBuffer = bytearray()
            #    print(self.__receiveBuffer)


            # Send data
            #print('s {:d}'.format(self.__sendBufferPtr))

        def doSingleSPIWrite(self, device, address, data):
            self.addSPIWriteCommand(device, address, data)
            self.doPending()

        def doSingleSPIRead(self, device, address):
            self.addSPIReadCommand(device, address)
            self.doPending()
            return self.__getResponse()[2]

        def addSPIWriteCommand(self, device, address, data):
            # Device from 0-127, higher number selects a device deeper in the serial chain
            # Register address from 0-255
            # 16b data block
            self.__addSPICommand((device << 8) | address, data)

        def addSPIReadCommand(self, device, address):
            # Read bit [15] set
            # Device from 0-127, higher number selects a device deeper in the serial chain
            # Register address from 0-255
            self.__addSPICommand(0x8000 | (device << 8) | address, 0)
            # This command will generate an SPI output at the other end of the chain

        def getSPIReadResponse(self):
            return self.__getResponse()[2]

if __name__ == '__main__':
    print('here')
    iface = interface(target='192.168.1.77')

    print('here')

    # Read^the UID on the ATSHA204A
    iface.atshaWake()
    uid = iface.atshaReadCfg(0, 4)
    uid = uid + iface.atshaReadCfg(2, 4)
    uid = uid + iface.atshaReadCfg(3, 4)
    print('Wiznet UID: 0x'+''.join(format(byte, '02x') for byte in reversed(uid)))
    iface.atshaSleep()

    print('here')
    
    print('There are {:d} devices in the SPI chain'.format(iface.numDevices()))

    print('here')
    # Set the network settings
    #iface.setNetworkSettings({
    #    'MAC'     : [0x00, 0x08, 0xDC, 0x01, 0x02, 0x04],
    #    'IP'      : [192, 168, 1, 76],
    #    'GATEWAY' : [192, 168, 1, 1],
    #    'SUBNET'  : [255, 255, 255, 0],
    #})

    # Read the current device IP / MAC etc.
    # If None, settings have not been programmed
    print('Flash network interface settings')
    ns = iface.getNetworkSettings()
    if ns == None:
        print('  DEFAULTS (FLASH UNPROGRAMMED)')
    else:
        print('  MAC:      0x{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}'.format(*ns['MAC']))
        print('  IP:       {:d}.{:d}.{:d}.{:d}'.format(*ns['IP']))
        print('  GATEWAY:  {:d}.{:d}.{:d}.{:d}'.format(*ns['GATEWAY']))
        print('  SUBNET:   {:d}.{:d}.{:d}.{:d}'.format(*ns['SUBNET']))

    print()
