// Technically not needed, just including anyway
#define OCLK_VALUE 16000000

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "netio.h"
#include <string.h>

#define IAP_ENTRY 0x1FFF1001            // Because Thum code
#define IAP_ERAS            0x010
//#define IAP_ERAS_DAT0       (IAP_ERAS + 0)
//#define IAP_ERAS_DAT1       (IAP_ERAS + 1)
//#define IAP_ERAS_SECT       (IAP_ERAS + 2)              
//#define IAP_ERAS_BLCK       (IAP_ERAS + 3)  
//#define IAP_ERAS_CHIP       (IAP_ERAS + 4)  
//#define IAP_ERAS_MASS       (IAP_ERAS + 5)  
#define IAP_PROG            0x022
#define DAT0_START_ADDR     0x0003FE00
#define DAT1_START_ADDR     0x0003FF00
#define CODE_TEST_ADDR      0x0001F000
#define BLOCK_SIZE          4096
#define SECT_SIZE           256

/* Private typedef -----------------------------------------------------------*/
enum COMMAND_TYPES {
	SPI_SEND_RECEIVE_COMMAND = 55,
	SPI_SEND_RECEIVE_RESPONSE = 1,
	
	FLASH_WRITE_COMMAND = 4,
	FLASH_WRITE_RESPONSE = 5,
	FLASH_READ_COMMAND = 6,
	FLASH_READ_RESPONSE = 7,
	
  I2C_COMMAND	= 8,
	I2C_RESPONSE = 9,
	
	ATSHA_WAKE_COMMAND = 10,
	ATSHA_WAKE_RESPONSE = 11,

	ATSHA_SLEEP_COMMAND = 12,
	ATSHA_SLEEP_RESPONSE = 13,
	
	ATSHA_READ_CFG_COMMAND = 14,
	ATSHA_READ_CFG_RESPONSE = 15,

	ATSHA_RANDOM_COMMAND = 16,
	ATSHA_RANDOM_RESPONSE = 17,
	
	GPIO_COMMAND = 18,
	GPIO_RESPONSE = 19,
	
	TIMEOUT_ERROR = 252,
	SPI_CRC_ERROR = 253,
  INSUFFICIENT_INPUT_DATA_ERROR = 254,
	UNKNOWN_COMMAND_ERROR = 255
};

/* Private define ------------------------------------------------------------*/
#define NUM_I2C_CHANNELS 4

GPIO_TypeDef * const I2C_SCL_BANK_MAP[NUM_I2C_CHANNELS] = {GPIOC, GPIOA, GPIOA, GPIOA};
uint16_t const I2C_SCL_PIN_MAP[NUM_I2C_CHANNELS] = {GPIO_Pin_11, GPIO_Pin_10, GPIO_Pin_0, GPIO_Pin_2};
GPIO_TypeDef * const I2C_SDA_BANK_MAP[NUM_I2C_CHANNELS] = {GPIOC, GPIOA, GPIOA, GPIOC};
uint16_t const I2C_SDA_PIN_MAP[NUM_I2C_CHANNELS] = {GPIO_Pin_10, GPIO_Pin_9, GPIO_Pin_1, GPIO_Pin_6};

#define NUM_GPIO_CHANNELS 14
GPIO_TypeDef * const GPIO_BANK_MAP[NUM_GPIO_CHANNELS] = {GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOC, GPIOB, GPIOB, GPIOB, GPIOB};
uint16_t const GPIO_PIN_MAP[NUM_GPIO_CHANNELS] = {GPIO_Pin_5, GPIO_Pin_4, GPIO_Pin_8, GPIO_Pin_9, GPIO_Pin_12, GPIO_Pin_13, GPIO_Pin_2, GPIO_Pin_3, GPIO_Pin_1, GPIO_Pin_0, GPIO_Pin_2, GPIO_Pin_3, GPIO_Pin_1, GPIO_Pin_0};

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t ATSHATimingDelay = 0;
static __IO uint32_t I2CTimingDelay = 0;
static __IO uint32_t TimingDelay = 0;
static __IO uint32_t LEDDelay = 0;
static uint8_t inputBuffer[DATA_BUF_SIZE];
static uint8_t *inputPosition, *inputEnd;
uint8_t outputBuffer[DATA_BUF_SIZE];
uint8_t *outputPosition;
int32_t inputCommandCount, outputResponseCount;

/* Private function prototypes -----------------------------------------------*/
void Network_Config();
void i2c_delay();
void TimingDelay_Decrement();
void GPIOInit();

int8_t processGPIOCommand();

int8_t processATSHAWakeCommand();
int8_t processATSHASleepCommand();
int8_t processATSHAReadCfgCommand();
int8_t processATSHARandomCommand();

int8_t processI2CCommand();

int8_t processFlashWriteCommand();
int8_t processFlashReadCommand();

int8_t processSPISendReceiveCommand();

void finalSPIReceive();

// Interrupt function
// NOTE: This function name is a magic name. It matches the expected name of the interrupt handler in a Cortex-M4.
// You will find a reference to the name in the startup file in the compiler tools for the particular processor, which is how
// the linker knows exactly where this function needs to go...
void SysTick_Handler(void) {
	TimingDelay_Decrement();
}

/* Private functions ---------------------------------------------------------*/

#define CRC16_POLY 0xA001

uint16_t calculateCRC16(uint8_t *data, uint32_t length, uint16_t init) {
  uint16_t crc = init;
  for (uint32_t i = 0; i < length; ++i) {
    crc ^= data[i];
    for (uint8_t j = 0; j < 8; ++j) {
      if (crc & 0x0001) {
        crc = (crc >> 1) ^ CRC16_POLY;
      } else {
        crc = crc >> 1;
      }
    }	
  }
  return crc;
}

// Need for ATSHA204 CRC
uint16_t reverseBits(uint16_t x) {
    x = (x >> 8) | (x << 8); // Swap the lower and upper 8 bits
    x = ((x & 0xF0F0) >> 4) | ((x & 0x0F0F) << 4); // Swap every 4 bits
    x = ((x & 0xCCCC) >> 2) | ((x & 0x3333) << 2); // Swap every 2 bits
    x = ((x & 0xAAAA) >> 1) | ((x & 0x5555) << 1); // Swap adjacent bits
    return x;
}

void DO_IAP( uint32_t id, uint32_t dst_addr, uint8_t* src_addr, uint32_t size)
{
    // Disable interrupts before calling IAP
    __disable_irq();
    
    // Call IAP Function
    ((void(*)(uint32_t,uint32_t,uint8_t*,uint32_t))IAP_ENTRY)( id,dst_addr,src_addr,size);

    // Enable interrupts
    __enable_irq();
}

void ClockInternal8MHz() {
	// Enable the internal RC oscillator
	*(volatile uint32_t *)(0x41001000) = 0x00000000;
	// PLL bypass enable
	*(volatile uint32_t *)(0x4100101C) = 0x00000001;
	// Set the PLL input clock source to the internal oscillator
	*(volatile uint32_t *)(0x41001020) = 0x00000000;	
	// PLL output enable
	// Not sure if we need this
	*(volatile uint32_t *)(0x41001018) = 0x00000001;

	// SysTick_Configuration - number of FCLK cycles per tick
	SysTick_Config(8000);

	// Set WZ_100US Register - TOE needs a 100us tick setting
	setTIC100US(800);
}

void ClockExternal48MHz() {
	// PLL power enable
	*(volatile uint32_t *)(0x41001010) = 0x00000001;
	// PLL bypass disable
	*(volatile uint32_t *)(0x4100101C) = 0x00000000;
	// PLL output enable
	*(volatile uint32_t *)(0x41001018) = 0x00000001;
	// Set the PLL input clock source to the external oscillator
	*(volatile uint32_t *)(0x41001020) = 0x00000001;

	// PLL frequency calculating register
	// [21:16] == M. M = M[5] x 32 + M[4] x 16 + M[3] x 8 + M[2] x 4 + M[1] x 2 + M[0] x 1 (2 ~ 63)
	// [13:8] == N. N = N[5] x 32 + N[4] x 16 + N[3] x 8 + N[2] x 4 + N[1] x 2 + N[0] x 1 (1 ~ 63)
	// [1:0] == OD. OD == 2 ^ (2 x OD[1]) x 2 ^ (1 x OD[0])
	// FOUT = FIN * M / (N x OD)
	// Default is M == 5, N == 2, OD == 1
	// We make this 6/2 => 48MHz.
	*(volatile uint32_t *)(0x41001014) = 0x00060200;

	// Set FCLK to output of PLL
	*(volatile uint32_t *)(0x41001030) = 0x00000001;
	// No FCLK prescale
	*(volatile uint32_t *)(0x41001034) = 0x00000000;

	// Set SSPCLK to output of PLL
	*(volatile uint32_t *)(0x41001040) = 0x00000001;
	// No SSPCLK prescale
	*(volatile uint32_t *)(0x41001034) = 0x00000000;	

	// Disable the internal RC oscillator (we don't use it)
	// Do this last so we don't turn off the clock before we have the new one running
	*(volatile uint32_t *)(0x41001000) = 0x00000001;

	// SysTick_Configuration - number of FCLK cycles per tick
	SysTick_Config(48000);

	// Set WZ_100US Register - TOE needs a 100us tick setting
	setTIC100US(4800);
}

int main(void) {
	  // This just sets up the clock system, but we're going to do it ourselves
	  // It was called in the reset handler anyway but I removed that as well
	  // NOTE: We do not use the Wiznet startup code - modifed version in user /src
    //SystemInit();

	  // Wiznet libraries are a joke, so we just bypass everything and write directly to the hardware registers
	  // Register offsets
	  // GPIOA == 0x42000000
	  // GPIOB == 0x43000000
	  // GPIOC == 0x44000000
	  // GPIOD == 0x45000000
	  // FLASH == 0x41005000
	  // DMA   == 0x41004000
	  // PADCON   == 0x41003000
	  // AFC/EXT1 == 0x41002000
	  // CRG == 0x41001000
	  // ADC == 0x41000000
    // UART1 == 0x4000D000
	  // UART0 == 0x4000C000
    // SSP1 == 0x4000B000
	  // SSP0 == 0x4000A000
	
		// Clock initialization
		ClockExternal48MHz();
		
		// GPIO configuration
    GPIOInit();
		
		// Wait one second for GPIO to stabilize
		// We latch the state of PB02 to determine whether to use default IP/MAC
		TimingDelay = 1000;
		while ( TimingDelay != 0 ) {}
		
		// Initialize the PHY
		PHY_Init(GPIOB, GPIO_Pin_14, GPIO_Pin_15);
		
		// Network configuration
    Network_Config();

	  // Modify TOE settings to optimize throughput
		
		// Resize the TCP buffers to 8kB for TX & RX on two sockets
		//uint8_t bufsize[8] = { 8, 8, 0, 0, 0, 0, 0, 0 };
		//wizchip_init(bufsize, bufsize);

		// TODO: We could use DMA to gain ~x2 throughput improvement,
		// but without a overlapping I/O protocol the improvement would be negligible
		// and the complexity would increase signficantly - do with next version

		// SSP0 interrupt mask
		// All interrupts disabled
		*(volatile uint32_t *)(0x4000A014) = 0x00000000;
		// SSP0 interrupt clear
		// Clear any previous interrupts
		*(volatile uint32_t *)(0x4000A020) = 0x00000003;	
		// SSP0 DMA mask
		// DMA for TX/RX FIFOs disabled
		*(volatile uint32_t *)(0x4000A024) = 0x00000000;
		// Set SSP0 clock prescaler to twelve (4MHz)
		*(volatile uint32_t *)(0x4000A010) = 0x0000000C;
		// SSP0 control register 0
		// [3:0] == Data size select (N-1, 4-16)
		// [5:4] == Frame format
		// [6] == Clock polarity
		// [7] == Clock phase
		// [15:8] == Serial clock rate (SCR). fSSPCLK = (CPSDVSR * (1 + SCR))
		// Set to 16 bit, Motorola SPI, rising edge polarity, first clock edge capture, 12MHz clock rate (divide by four, SCR == 0)
	  *(volatile uint32_t *)(0x4000A000) = 0x0000000F;	
		// SSP0 control register 1
		// [0] == Loop back enable
		// [1] == Port enable
		// [2] == Master == 0, Slave == 1
		// [3] == Slave output disable
		// Set to normal mode, enabled, master, slave output disabled
	  *(volatile uint32_t *)(0x4000A004) = 0x00000002;	

/*
		// SSP1 interrupt mask
		// All interrupts disabled
		*(volatile uint32_t *)(0x4000B014) = 0x00000000;
		// SSP0 interrupt clear
		// Clear any previous interrupts
		*(volatile uint32_t *)(0x4000B020) = 0x00000003;	
		// SSP0 DMA mask
		// DMA for TX/RX FIFOs disabled
		*(volatile uint32_t *)(0x4000B024) = 0x00000000;
		// Set SSP0 clock prescaler to twelve (4MHz)
		*(volatile uint32_t *)(0x4000B010) = 0x0000000C;
		// SSP0 control register 0
		// [3:0] == Data size select (N-1, 4-16)
		// [5:4] == Frame format
		// [6] == Clock polarity
		// [7] == Clock phase
		// [15:8] == Serial clock rate (SCR). fSSPCLK = (CPSDVSR * (1 + SCR))
		// Set to 16 bit, Motorola SPI, rising edge polarity, first clock edge capture, 4MHz clock rate (divide by twelve, SCR == 0)
	  *(volatile uint32_t *)(0x4000B000) = 0x0000000F;	
		// SSP0 control register 1
		// [0] == Loop back enable
		// [1] == Port enable
		// [2] == Master == 0, Slave == 1
		// [3] == Slave output disable
		// Set to normal mode, enabled, master, slave output disabled
	  *(volatile uint32_t *)(0x4000B004) = 0x00000002;	
	*/

		// SSP1 interrupt mask
		// All interrupts disabled
		*(volatile uint32_t *)(0x4000B014) = 0x00000000;
		// SSP1 interrupt clear
		// Clear any previous interrupts
		*(volatile uint32_t *)(0x4000B020) = 0x00000003;	
		// SSP1 DMA mask
		// DMA for TX/RX FIFOs disabled
		*(volatile uint32_t *)(0x4000B024) = 0x00000000;
		// Set SSP1 clock prescaler to twelve (4MHz)
		*(volatile uint32_t *)(0x4000B010) = 0x0000000C;
		// SSP1 control register 0
		// [3:0] == Data size select (N-1, 4-16)
		// [5:4] == Frame format
		// [6] == Clock polarity
		// [7] == Clock phase
		// [15:8] == Serial clock rate (SCR). fSSPCLK = (CPSDVSR * (1 + SCR))
		// Set to 16 bit, Motorola SPI, rising edge polarity, first clock edge capture, 4MHz clock rate (divide by twelve, SCR == 0)
	  *(volatile uint32_t *)(0x4000B000) = 0x0000000F;	
		// SSP1 control register 1
		// [0] == Loop back enable
		// [1] == Port enable
		// [2] == Master == 0, Slave == 1
		// [3] == Slave output disable
		// Set to normal mode, enabled, slave, slave output disabled
	  *(volatile uint32_t *)(0x4000B004) = 0x00000006;

		// Initialise the offsets
		inputPosition = inputBuffer;
		inputEnd = inputBuffer;
		
    while (1) {
      loopback_tcps(0, inputBuffer, 7);			// Loopback TCP on port 7
			int32_t l = rpacket_tcps(1, inputBuffer+(inputEnd-inputPosition), DATA_BUF_SIZE-(inputEnd-inputPosition), 50010); // Commands on port 50010 via TCP
			if ( l < 1 ) continue;

			int8_t response = 1;

			// Initialise the buffer locations
			inputEnd += l;
			inputPosition = inputBuffer;
			inputCommandCount = 0;
			outputPosition = outputBuffer;
			outputResponseCount = 0;
			
			// Flush the SPI receiver buffer first before running any commands
			while ( (*(volatile uint32_t *)(0x4000B00C) & 4) ) {
				uint32_t temp = *(volatile uint32_t *)(0x4000B008);
			}
			
			// Command processing loop
			while ( inputPosition != inputEnd ) {
			
				// Start a one second timeout counter
				// We expect all responses to have been sent back by this point
				TimingDelay = 1000;
				
				// Decode the command type and process it
				// We validate the format of the command first
				switch(*((uint8_t *)inputPosition)) {
					case SPI_SEND_RECEIVE_COMMAND:
						response = processSPISendReceiveCommand();
						break;
					// This command is a flash write command
					case FLASH_WRITE_COMMAND:
						response = processFlashWriteCommand();
						break;
					// This command is a flash read command
					case FLASH_READ_COMMAND:
						response = processFlashReadCommand();
						break;
					// This command is an I2C command
					case I2C_COMMAND:
						response = processI2CCommand();
						break;
					// ATSHA204 commands
					case ATSHA_WAKE_COMMAND:
						response = processATSHAWakeCommand();
						break;
					case ATSHA_SLEEP_COMMAND:
						response = processATSHASleepCommand();
						break;
					case ATSHA_READ_CFG_COMMAND:
						response = processATSHAReadCfgCommand();
						break;
					case ATSHA_RANDOM_COMMAND:
						response = processATSHARandomCommand();
						break;
					case GPIO_COMMAND:
						response = processGPIOCommand();
						break;
					default:
						response = -1;
						break;
				}
				
				// We are short sufficient bytes to continue so we mark where we got to and try to get more from the network stack
				if ( response == -3 ) {
					// Potentially overlapping - don't use memcpy
					// TODO: Optimise?
					memmove(inputBuffer, inputPosition, inputEnd-inputPosition);
					inputEnd = inputBuffer + (inputEnd - inputPosition);
					inputPosition = inputBuffer;
					response = 0;
					break;
				}
				
				// Send an unknown command response if anything went wrong in the validation process
				// Break the loop at this point without continuing
				// Additional information can be encoded using the response code (TODO)
				// Flush the input buffer
				if ( response < 0 ) {
					outputBuffer[0] = UNKNOWN_COMMAND_ERROR;
					outputBuffer[1] = response;
					xpacket_tcps(1, outputBuffer, 2, 50010);
					break;
				}
	
				// If we timed out, send a timeout message and flush the input buffer
				if ( TimingDelay == 0 ) {
					outputBuffer[0] = TIMEOUT_ERROR;
					xpacket_tcps(1, outputBuffer, 1, 50010);
					response = -2;
					break;
				}

			}
				
			// Skip the final collection & send if we saw an error and flush the buffer
			if ( response < 0 ) {
				inputPosition = inputBuffer;
				inputEnd = inputBuffer;
				continue;
			}
		
			// Start a one second timeout counter
			// We expect all responses to have been sent back by this point
			TimingDelay = 1000;
			
			// Collect any remaining SPI packets
			finalSPIReceive();
							
			// If we timed out, send a timeout message and flush the input buffer
			if ( TimingDelay == 0 ) {
				outputBuffer[0] = TIMEOUT_ERROR;
				xpacket_tcps(1, outputBuffer, 1, 50010);
				inputPosition = inputBuffer;
				inputEnd = inputBuffer;
				continue;
			}
				
			// Otherwise, send the responses
  		xpacket_tcps(1, outputBuffer, outputPosition - outputBuffer, 50010);
			
			// Reset the pointer position if they are aligned
			if ( inputPosition == inputEnd ) {
				inputPosition = inputBuffer;
				inputEnd = inputBuffer;				
			}

		}
	
	return 0;
}

void finalSPIReceive() {
	uint16_t temp;
	// Loop until the number of responses matches the number of commands
	while (outputResponseCount != inputCommandCount) {
		if ( (*(volatile uint32_t *)(0x4000B00C) & 4) ) {
			// Response byte
			*outputPosition = SPI_SEND_RECEIVE_RESPONSE;
			outputPosition++;
			// Add the first 16 bits
			temp = *(volatile uint32_t *)(0x4000B008);
			*outputPosition = temp >> 8;
			outputPosition++;
			*outputPosition = temp & 0xFF;
			outputPosition++;
			// We need three 16b blocks, so we stall here for the second one.
			// The timeout can break the loop
			while ( !(*(volatile uint32_t *)(0x4000B00C) & 4) ) {
				if ( TimingDelay == 0 ) return;
			}
			// Add the second 16 bits
			temp = *(volatile uint32_t *)(0x4000B008);
			*outputPosition = temp >> 8;
			outputPosition++;
			*outputPosition = temp & 0xFF;
			outputPosition++;
			while ( !(*(volatile uint32_t *)(0x4000B00C) & 4) ) {
				if ( TimingDelay == 0 ) return;
			}
			// Add the third 16 bits
			temp = *(volatile uint32_t *)(0x4000B008);
			*outputPosition = temp >> 8;
			outputPosition++;
			*outputPosition = temp & 0xFF;
			outputPosition++;
			
			// Calculate the CRC & create a CRC error message if the CRC in the data doesn't match the calculation
		  //if ( calculateCRC16(outputPosition-4, 4, 0xFFFF) != temp ) {
		//		outputPosition -= 5;
		//		*outputPosition = SPI_CRC_ERROR;
		//		outputPosition++;
		//	}
			
			// Increment the response counter
			outputResponseCount++;
		}
		// Break the loop on timeout
		if ( TimingDelay == 0 ) break;
	}
}

int8_t processFlashReadCommand() {
	// Need 2 bytes
	// First byte is command, second is read length
	if ((inputEnd-inputPosition) < 2)	{
  		return -3;
  }
	
	// Consume the input data and store the results
	inputPosition++;
	uint32_t size = *inputPosition;
	inputPosition++;
	
	// Initialize response
	*outputPosition = FLASH_READ_RESPONSE;
	outputPosition++;
	*outputPosition = size;
	outputPosition++;
	
	// Copy the flash data
	memcpy(outputPosition, (uint8_t *)DAT0_START_ADDR, size+1);
	outputPosition += size+1;
	
	return 0;
}


int8_t processFlashWriteCommand() {
	// Need 2 bytes
	// First byte is command, second is read length
	if ((inputEnd-inputPosition) < 2)	{
  		return -3;
  }
	
	// Consume the input data and store the results
	inputPosition++;
	uint32_t size = *inputPosition;
	inputPosition++;
	
	// Check there is enough data left to consume
	if ((inputEnd-inputPosition) < size) {
		inputPosition -= 2;
		return -3;
	}
	
	// Due to silicon issue, we have to drop back to the RC oscillator
	// and 8MHz while we run the FLASH update, then return to 48MHz
	ClockInternal8MHz();
	
	// Erase DAT0 then program it
  DO_IAP(IAP_ERAS_DAT0, 0, 0, 0);
  DO_IAP(IAP_PROG, DAT0_START_ADDR, inputPosition, size+1);
	inputPosition += size+1;
	
	// Move the clock system back to the internal oscillator
	ClockExternal48MHz();
	
	// TODO: Generate response
	*outputPosition = FLASH_WRITE_RESPONSE;
	outputPosition++;
	*outputPosition = size;
	outputPosition++;
	
	return 0;
}


int8_t processSPISendReceiveCommand() {
	// Need 5 bytes
	if ((inputEnd-inputPosition) < 5)	{
  		return -3;
  }
	
	// At this point we don't need safety checks
	// because we already validated the data the first pass

	// Run the transmit loop while collecting any received SPI commands in parallel

	// The SPI command block is pretty straightforward
	// We write in blocks of 16 bits and flow control the output
	// We simultaneously collect blocks from the receiver on SSP1
	// We discard the first two bytes (one was the command, the other is to maintain 16b byte alignment)
	// SSP0 buffer status 
	// [0] == Transmit FIFO not empty
	// [1] == Transmit FIFO full
	// [2] == Receive FIFO not empty
	// [3] == Receive FIFO full
	// [4] == SPI busy
	
	uint16_t temp;
	
	// If there is data in the RX buffer, receive it - we don't want to overflow
	// TODO: Check overflow
	if ( (*(volatile uint32_t *)(0x4000B00C) & 4) ) {
		// Response byte
		*outputPosition = SPI_SEND_RECEIVE_RESPONSE;
		outputPosition++;
		// Add the first 16 bits
		temp = *(volatile uint32_t *)(0x4000B008);
		*outputPosition = temp >> 8;
		outputPosition++;
		*outputPosition = temp & 0xFF;
		outputPosition++;
		// We need three 16b blocks, so we stall here for the second one.
		// The timeout can break the loop
		while ( !(*(volatile uint32_t *)(0x4000B00C) & 4) ) {
			if ( TimingDelay == 0 ) return -2;
		}
		// Add the second 16 bits
		temp = *(volatile uint32_t *)(0x4000B008);
		*outputPosition = temp >> 8;
		outputPosition++;
		*outputPosition = temp & 0xFF;
		outputPosition++;
		while ( !(*(volatile uint32_t *)(0x4000B00C) & 4) ) {
			if ( TimingDelay == 0 ) return -2;
		}
		// Add the third 16 bits
		temp = *(volatile uint32_t *)(0x4000B008);
		*outputPosition = temp >> 8;
		outputPosition++;
		*outputPosition = temp & 0xFF;
		outputPosition++;
		
		// Calculate the CRC & create a CRC error message if the CRC in the data doesn't match the calculation
		//if ( calculateCRC16(outputPosition-4, 4, 0xFFFF) != temp ) {
		//	outputPosition -= 5;
		//  *outputPosition = SPI_CRC_ERROR;
	//		outputPosition++;
		//}
		
		// Increment the response counter
		outputResponseCount++;
	}

	// Skip over the command byte
	inputPosition++;
	
	// Calculate the CRC
	uint16_t crc = calculateCRC16(inputPosition, 4, 0xFFFF);
	
	// Wait until the transmit FIFO isn't full
	while ( !(*(volatile uint32_t *)(0x4000A00C) & 2) ) {}
	// SSP0 data register (write to transmit, read to receive)
	temp = ((uint16_t)(*inputPosition << 8)) | *(inputPosition + 1);
	*(volatile uint32_t *)(0x4000A008) = temp;
	inputPosition += 2;
	// Wait until the transmit FIFO isn't full
	while ( !(*(volatile uint32_t *)(0x4000A00C) & 2) ) {}
	// SSP0 data register (write to transmit, read to receive)
	temp = ((uint16_t)(*inputPosition << 8)) | *(inputPosition + 1);
	*(volatile uint32_t *)(0x4000A008) = temp;
	inputPosition += 2;
	// Wait until the transmit FIFO isn't full
	while ( !(*(volatile uint32_t *)(0x4000A00C) & 2) ) {}
	// SSP0 data register (write to transmit, read to receive)
	*(volatile uint32_t *)(0x4000A008) = crc;
	// Increment the command counter
	inputCommandCount++;
	
	return 0;
}

// I2C functions
void I2CSetSCL(uint8_t channel, uint8_t value) {

	//GPIO_InitTypeDef GPIO_InitDef;
	//GPIO_InitDef.GPIO_Pad = GPIO_PuPd_UP; // Pullup
  //GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

	// SCL
	if ( value == 0 ) {
	
		// Output (already set to 0)
	  //GPIO_InitDef.GPIO_Direction = GPIO_Direction_OUT;
		//GPIO_InitDef.GPIO_Pin = I2C_SCL_PIN_MAP[channel];
		//GPIO_Init(I2C_SCL_BANK_MAP[channel], &GPIO_InitDef);
		I2C_SCL_BANK_MAP[channel]->OUTENSET = I2C_SCL_PIN_MAP[channel];

	} else {
		
		// Input
		//GPIO_InitDef.GPIO_Direction = GPIO_Direction_IN;
		//GPIO_InitDef.GPIO_Pin = I2C_SCL_PIN_MAP[channel];
		//GPIO_Init(I2C_SCL_BANK_MAP[channel], &GPIO_InitDef);
		//GPIO_SetBits(I2C_SCL_BANK_MAP[channel], I2C_SCL_PIN_MAP[channel]);
		I2C_SCL_BANK_MAP[channel]->OUTENCLR = I2C_SCL_PIN_MAP[channel];
		
	}

}

void I2CSetSDA(uint8_t channel, uint8_t value) {

	//GPIO_InitTypeDef GPIO_InitDef;
	//GPIO_InitDef.GPIO_Pad = GPIO_PuPd_UP; // Pullup
  //GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

	if ( value == 0 ) {
	  
		// Output (already set to 0)
		//GPIO_InitDef.GPIO_Direction = GPIO_Direction_OUT;
		//GPIO_InitDef.GPIO_Pin = I2C_SDA_PIN_MAP[channel];
		//GPIO_Init(I2C_SDA_BANK_MAP[channel], &GPIO_InitDef);
		//GPIO_ResetBits(I2C_SDA_BANK_MAP[channel], I2C_SDA_PIN_MAP[channel]);
		I2C_SDA_BANK_MAP[channel]->OUTENSET = I2C_SDA_PIN_MAP[channel];
		
	} else {
		
		// Input
		//GPIO_InitDef.GPIO_Direction = GPIO_Direction_IN;
		//GPIO_InitDef.GPIO_Pin = I2C_SDA_PIN_MAP[channel];
		//GPIO_Init(I2C_SDA_BANK_MAP[channel], &GPIO_InitDef);
		//GPIO_SetBits(I2C_SDA_BANK_MAP[channel], I2C_SDA_PIN_MAP[channel]);
		I2C_SDA_BANK_MAP[channel]->OUTENCLR = I2C_SDA_PIN_MAP[channel];
		
	}

}

uint8_t I2CGetSDA(uint8_t channel) {
	
	// Write the output packet
  if ( GPIO_ReadInputDataBit(I2C_SDA_BANK_MAP[channel], I2C_SDA_PIN_MAP[channel]) == Bit_SET ) {
		return 1;
	} else {
		return 0;
	}
	
}

uint8_t i2c_clk(uint8_t channel, uint8_t bit) {
		uint8_t res;
	
		// Bring clock low and set data bit
		I2CSetSCL(channel, 0);
		I2CSetSDA(channel, bit);
    i2c_delay();
	
		//Set clock high & sample bit
		I2CSetSCL(channel, 1);
		res = I2CGetSDA(channel);
	  i2c_delay();

    // Bring clock low and data low
		I2CSetSCL(channel, 0);
	  I2CSetSDA(channel, 0);
   	i2c_delay();
	
	return res;
}

void i2c_repeated_start(uint8_t channel) {

		// Bring data high
		I2CSetSDA(channel, 1);
	  i2c_delay();
	
    // Bring clock high
    I2CSetSCL(channel, 1);
	  i2c_delay();
	
    // Bring data low
    I2CSetSDA(channel, 1);
	  i2c_delay();
	
    // Bring clock low
    I2CSetSCL(channel, 0);
	  i2c_delay();

}

void i2c_start(uint8_t channel) {

		// Make sure clock and data are high
		I2CSetSDA(channel, 1);
		I2CSetSCL(channel, 1);
	  i2c_delay();
	
    // Bring data low
		I2CSetSDA(channel, 0);
	  i2c_delay();
	
    // Bring clock low
		I2CSetSCL(channel, 0);
	  i2c_delay();

}

void i2c_stop(uint8_t channel) {
	
    // Bring clock high (data should already be low)
		I2CSetSCL(channel, 1);
		i2c_delay();
	
    // Bring data high with clock high
    I2CSetSDA(channel, 1);
	  i2c_delay();
}

void i2c_write(uint8_t channel, uint8_t value) {
	uint8_t temp;
	for ( uint8_t i = 0; i != 8 ; i++ ) {
		i2c_clk(channel, (value >> (7-i)) & 0x1);
	}
}

uint8_t i2c_read(uint8_t channel) {
	uint8_t temp, bit;
  uint16_t result = 0;
	for ( uint8_t i = 0; i != 8 ; i++ ) {
    bit = i2c_clk(channel, 1);
    result = (result << 1) | bit;
	}
	return result;
}

uint8_t i2c_check_ack(uint8_t channel) {
	uint8_t sdaout;
  sdaout = i2c_clk(channel, 1);
	i2c_delay();
	return sdaout;
}

// Simple delay
// NOTE: This was set experimentally to result in approx. 25kHz operation
// This is slow enough that it generally works with the internal pullups
// You can go faster by reducing the delay but that comes with the risk
// of insufficient pullup strength
void i2c_delay() {
	uint32_t delay = 20;
	while (--delay);
}

int8_t processI2CCommand() {
	// COMMAND
	// CHANNEL
	// SLAVE ADDRESS
	// WRITE BYTE COUNT
	// READ BYTE COUNT
	// [WRITE DATA 8b x WRITE BYTE COUNT]
	
	// Need 4 bytes for the header
	if ((inputEnd-inputPosition) < 5)	{
  		return -3;
  }
	
	// Extract the information
	uint8_t channel = *(inputPosition + 1);
	uint8_t slaveAddress = *(inputPosition + 2);
	uint8_t writeByteCount = *(inputPosition + 3);
	uint8_t readByteCount = *(inputPosition + 4);
	
	// Check we have enough data
	if ((inputEnd-inputPosition) < writeByteCount + 5)	{
  		return -3;
  }
	
	// Move the pointer forward
	inputPosition += 5;
	
	uint8_t res, data[256];
	memcpy(data, inputPosition, writeByteCount);
	inputPosition += writeByteCount;
	
	// Write portion
	i2c_start(channel);
	i2c_write(channel, slaveAddress << 1);
	if ( i2c_check_ack(channel) == 1 ) {
  	res = 1;
		goto i2cFault;
	}
	for (uint8_t i = 0 ; i < writeByteCount ; ++i) {
		i2c_write(channel, data[i]);
		if ( i2c_check_ack(channel) == 1 ) {
			res = 2;
			goto i2cFault;
		}
	}
	i2c_stop(channel);
	if (readByteCount) {
		i2c_start(channel);
		//i2c_repeated_start(channel);
		i2c_write(channel, (slaveAddress << 1) | 1);
		if ( i2c_check_ack(channel) == 1 ) {
			res = 3;
			goto i2cFault;
		}
		for (uint8_t i = 0 ; i < readByteCount-1 ; ++i) {
			data[i] = i2c_read(channel);
			i2c_clk(channel, 0); // Master ack			
		}
		data[readByteCount-1] = i2c_read(channel);
		i2c_clk(channel, 1); // Master nack			
		i2c_stop(channel);
	}

  // Response	
	*outputPosition = I2C_RESPONSE;
	outputPosition++;
	*outputPosition = 0;
	outputPosition++;
	*outputPosition = channel;
	outputPosition++;
	*outputPosition = slaveAddress;
	outputPosition++;
	*outputPosition = writeByteCount;
	outputPosition++;
	*outputPosition = readByteCount;
	outputPosition++;

	//if (readByteCount) {
	memcpy(outputPosition, data, readByteCount);
	outputPosition += readByteCount;
	//}
	
	return 0;
							
i2cFault:
  i2c_stop(channel);
	// While this is a write we note the fault signal
	*outputPosition = I2C_RESPONSE;
	outputPosition++;
	*outputPosition = res;
	outputPosition++;
	*outputPosition = channel;
	outputPosition++;
	*outputPosition = slaveAddress;
	outputPosition++;
	*outputPosition = writeByteCount;
	outputPosition++;
	*outputPosition = readByteCount;
	outputPosition++;
	return 0;
}

int8_t processATSHAWakeCommand() {
	inputPosition++;
	uint8_t addr = 0xC8;
	uint8_t res;
	
	// Transaction wakes device
	// Wait 60us after start minimum
	i2c_start(0);
  I2CTimingDelay = 2;
  while(I2CTimingDelay != 0);
	i2c_stop(0);
	
	// Wait additional 2.5ms minimum
	I2CTimingDelay = 4;
  while(I2CTimingDelay != 0);
	
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 1;
		goto atshaWakeFault;
	}
	res = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);
	
	if (res != 4) {
		res = 2;
		goto atshaWakeFault;
	}
	
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 3;
		goto atshaWakeFault;
	}
	res = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);
	
	if (res != 0x11) {
		res = 4;
		goto atshaWakeFault;
	}
	
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 5;
		goto atshaWakeFault;
	}
	res = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);
	
	if (res != 0x33) {
		res = 6;
		goto atshaWakeFault;
	}
	
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 7;
		goto atshaWakeFault;
	}
	res = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);
	
	if (res != 0x43) {
		res = 8;
		goto atshaWakeFault;
	}
	
	// While this is a write we note the fault signal
	*outputPosition = ATSHA_WAKE_RESPONSE;
	outputPosition++;
	*outputPosition = 0;
	outputPosition++;
	return 0;
	
atshaWakeFault:
  i2c_stop(0);
	// While this is a write we note the fault signal
	*outputPosition = ATSHA_WAKE_RESPONSE;
	outputPosition++;
	*outputPosition = res;
	outputPosition++;
	return 0;
}

int8_t processATSHASleepCommand() {
	inputPosition++;
	uint8_t addr = 0xC8;
	uint8_t res;
	
	i2c_start(0);
	i2c_write(0, addr);
	if ( i2c_check_ack(0) == 1 ) {
		res = 1;
		goto atshaSleepFault;
	}
	i2c_write(0, 1); // Sleep command
	if ( i2c_check_ack(0) == 1 ) {
		res = 2;
		goto atshaSleepFault;
	}
	i2c_stop(0);
	
	// While this is a write we note the fault signal
	*outputPosition = ATSHA_SLEEP_RESPONSE;
	outputPosition++;
	*outputPosition = 0;
	outputPosition++;
	return 0;
	
atshaSleepFault:
	i2c_stop(0);
	*outputPosition = ATSHA_SLEEP_RESPONSE;
	outputPosition++;
	*outputPosition = res;
	outputPosition++;
	return 0;	
}

int8_t processATSHAReadCfgCommand() {
	// Need 2 bytes to begin processing
	if ((inputEnd-inputPosition) < 3)	{
  		return -3;
  }

	inputPosition++;
	uint8_t registerAddress = *inputPosition;
	inputPosition++;
	uint8_t responseCount = *inputPosition;
	inputPosition++;
	
	uint8_t addr = 0xC8;
	uint8_t word = 0x3;
	uint8_t count = 0x7;
	uint8_t cmd = 0x2;
	uint8_t block[] = {count, cmd, 0, registerAddress, 0};
	uint16_t crc = reverseBits(calculateCRC16(block, sizeof(block), 0));

	uint8_t res;
	
	i2c_start(0);
  i2c_write(0, addr);
	if ( i2c_check_ack(0) == 1 ) {
		res = 1;
		goto atshaReadCfgFault;
	}
	i2c_write(0, word);
	if ( i2c_check_ack(0) == 1 ) {
		res = 2;
		goto atshaReadCfgFault;
	}
	i2c_write(0, count); // count + crc(2) + opcode + param1 + param2(2)
	if ( i2c_check_ack(0) == 1 ) {
		res = 3;
		goto atshaReadCfgFault;
	}
	i2c_write(0, cmd); // 0x2 == READ
	if ( i2c_check_ack(0) == 1 ) {
		res = 4;
		goto atshaReadCfgFault;
	}
	i2c_write(0, 0); // param1 (mode bits, 4 byte read, configuration area)
	if ( i2c_check_ack(0) == 1 ) {
		res = 5;
		goto atshaReadCfgFault;
	}
	i2c_write(0, registerAddress); // param2 lsb (register address)
	if ( i2c_check_ack(0) == 1 ) {
		res = 6;
		goto atshaReadCfgFault;
	}
	i2c_write(0, 0); // param2 msb
	if ( i2c_check_ack(0) == 1 ) {
		res = 7;
		goto atshaReadCfgFault;
	}
	i2c_write(0, crc & 0xFF); //(crc >> 8) & 0xFF); // crc lsb
	if ( i2c_check_ack(0) == 1 ) {
		res = 8;
		goto atshaReadCfgFault;
	}
	i2c_write(0, (crc >> 8) & 0xFF); // crc & 0xFF); // crc msb
	if ( i2c_check_ack(0) == 1 ) {
		res = 9;
		goto atshaReadCfgFault;
	}
	i2c_stop(0);
	
	// Wait 4ms minimum
	I2CTimingDelay = 5;
  while(I2CTimingDelay != 0);

	// Start read response
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 10;
		goto atshaReadCfgFault;
	}
	uint8_t tmp[256];
	tmp[0] = i2c_read(0);
	i2c_clk(0, 0);
	if ( tmp[0] != responseCount+3 ) {
		res = 11;
		goto atshaReadCfgFault;
	}
	
	// Loop the read
	for (uint8_t i = 1 ; i < tmp[0]-1 ; ++i) {
		tmp[i] = i2c_read(0);
		i2c_clk(0, 0);
	}

	// Last byte & nack
	tmp[tmp[0]-1] = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);

	uint16_t resCRC = (((uint16_t)(tmp[tmp[0]-1])) << 8) | (uint16_t)(tmp[tmp[0]-2]);
	uint16_t calcResCRC = reverseBits(calculateCRC16(tmp, tmp[0]-2, 0));
	if ( calcResCRC != resCRC ) {
		res = 12;
		goto atshaReadCfgFault;	
	}

	// While this is a write we note the fault signal
	*outputPosition = ATSHA_READ_CFG_RESPONSE;
	outputPosition++;
	*outputPosition = 0;
	outputPosition++;
	*outputPosition = tmp[0] - 3;
	outputPosition++;
	memcpy(outputPosition, tmp+1, tmp[0] - 3);
	outputPosition += tmp[0] - 3;
	return 0;
	
atshaReadCfgFault:
	i2c_stop(0);
	*outputPosition = ATSHA_READ_CFG_RESPONSE;
	outputPosition++;
	*outputPosition = res;
	outputPosition++;
	return 0;	
}


int8_t processATSHARandomCommand() {
	inputPosition++;
	
	uint8_t addr = 0xC8;
	uint8_t word = 0x3;
	uint8_t count = 0x7;
	uint8_t cmd = 0x1B;
	uint8_t block[] = {count, cmd, 0, 0, 0};
	uint16_t crc = reverseBits(calculateCRC16(block, sizeof(block), 0));

	uint8_t res;

	i2c_start(0);
  i2c_write(0, addr);
	if ( i2c_check_ack(0) == 1 ) {
		res = 1;
		goto atshaRandomFault;
	}
	i2c_write(0, word);
	if ( i2c_check_ack(0) == 1 ) {
		res = 2;
		goto atshaRandomFault;
	}
	i2c_write(0, count); // count + crc(2) + opcode + param1 + param2(2)
	if ( i2c_check_ack(0) == 1 ) {
		res = 3;
		goto atshaRandomFault;
	}
	i2c_write(0, cmd); // 0x1B == RANDOM
	if ( i2c_check_ack(0) == 1 ) {
		res = 4;
		goto atshaRandomFault;
	}
	i2c_write(0, 0); // param1
	if ( i2c_check_ack(0) == 1 ) {
		res = 5;
		goto atshaRandomFault;
	}
	i2c_write(0, 0); // param2 lsb
	if ( i2c_check_ack(0) == 1 ) {
		res = 6;
		goto atshaRandomFault;
	}
	i2c_write(0, 0); // param2 msb
	if ( i2c_check_ack(0) == 1 ) {
		res = 7;
		goto atshaRandomFault;
	}
	i2c_write(0, crc & 0xFF);
	if ( i2c_check_ack(0) == 1 ) {
		res = 8;
		goto atshaRandomFault;
	}
	i2c_write(0, (crc >> 8) & 0xFF);
	if ( i2c_check_ack(0) == 1 ) {
		res = 9;
		goto atshaRandomFault;
	}
	i2c_stop(0);
	
	// Wait 50ms minimum
	I2CTimingDelay = 51;
  while(I2CTimingDelay != 0);

	// Start read response
	i2c_start(0);
	i2c_write(0, addr | 1);
	if ( i2c_check_ack(0) == 1 ) {
		res = 10;
		goto atshaRandomFault;
	}
	uint8_t tmp[256];
	tmp[0] = i2c_read(0);
	i2c_clk(0, 0);
	if ( tmp[0] != 35 ) {
		res = 11;
		goto atshaRandomFault;
	}
	
	// Loop the read
	for (uint8_t i = 1 ; i < tmp[0]-1 ; ++i) {
		tmp[i] = i2c_read(0);
		i2c_clk(0, 0);
	}

	// Last byte & nack
	tmp[tmp[0]-1] = i2c_read(0);
	i2c_clk(0, 1);
	i2c_stop(0);

	uint16_t resCRC = (((uint16_t)(tmp[tmp[0]-1])) << 8) | (uint16_t)(tmp[tmp[0]-2]);
	uint16_t calcResCRC = reverseBits(calculateCRC16(tmp, tmp[0]-2, 0));
	if ( calcResCRC != resCRC ) {
		res = 12;
		goto atshaRandomFault;	
	}

	// While this is a write we note the fault signal
	*outputPosition = ATSHA_RANDOM_RESPONSE;
	outputPosition++;
	*outputPosition = 0;
	outputPosition++;
	*outputPosition = tmp[0] - 3;
	outputPosition++;
	memcpy(outputPosition, tmp+1, tmp[0] - 3);
	outputPosition += tmp[0] - 3;
	return 0;
	
atshaRandomFault:
	i2c_stop(0);
	*outputPosition = ATSHA_RANDOM_RESPONSE;
	outputPosition++;
	*outputPosition = res;
	outputPosition++;
	return 0;	
}

int8_t processGPIOCommand() {
	// COMMAND
	// MODE [7:5], PIN[4:1], VALUE[0]
	
	// MODE[2:0] == 0x0 == Read input value
	// MODE[2:0] == 0x1 == Set output value
	// MODE[2:0] == 0x2 == Set input / output
	// MODE[2:0] == 0x3 == Disable pullup
	// MODE[2:0] == 0x4 == Pullup / pulldown
	
	// Need 2 bytes to begin processing
	if ((inputEnd-inputPosition) < 2)	{
  		return -3;
  }

	inputPosition++;
	uint8_t value = *inputPosition;
	inputPosition++;
	uint8_t pin = (value >> 1) & 0xF;
	uint8_t mode = (value >> 5) & 0x7;

	// Initialise the response with the default for non-read transactions
	*outputPosition = GPIO_RESPONSE;
	outputPosition++;
	*outputPosition = value;
	outputPosition++;

	value = value & 1;

	if ( mode == 1 ) {
		// Set output value

		// Loop through pins until nothing left to do
		if (value) {
			GPIO_SetBits(GPIO_BANK_MAP[pin], GPIO_PIN_MAP[pin]);
		} else {
			GPIO_ResetBits(GPIO_BANK_MAP[pin], GPIO_PIN_MAP[pin]);						
		}

	} else if ( mode == 2 ) {
		// Set input (0) / output (1) mode

		// Loop through pins until nothing left to do
		if (value) {
			GPIO_BANK_MAP[pin]->OUTENSET = GPIO_PIN_MAP[pin];
		} else {
			GPIO_BANK_MAP[pin]->OUTENCLR = GPIO_PIN_MAP[pin];
		}
		
	} else if ( mode == 3 ) {
		// Disable pullup / pulldown
		
		// Find the pos
		uint8_t pos = 0;
		while ((GPIO_PIN_MAP[pin] >> pos) != 1) {
			pos++;
		}
		
		// Read pad settings and mask out the pullup mode (1:0)
		uint8_t pad_value = GPIO_Read_PinPad(GPIO_BANK_MAP[pin], pos) & 0xFC;
		pad_value |= GPIO_PuPd_NOPULL;
		GPIO_PinPadConfig(GPIO_BANK_MAP[pin], pos, pad_value);
		
	} else if ( mode == 4 ) {
		// Enable pullup resistor
		
		// Find the pos
		uint8_t pos = 0;
		while ((GPIO_PIN_MAP[pin] >> pos) != 1) {
			pos++;
		}
		
		// Read pad settings and mask out the pullup mode (1:0)
		uint8_t pad_value = GPIO_Read_PinPad(GPIO_BANK_MAP[pin], pos) & 0xFC;
		if (value) {
			pad_value |= GPIO_PuPd_UP;
		} else {
			pad_value |= GPIO_PuPd_DOWN;			
		}
		GPIO_PinPadConfig(GPIO_BANK_MAP[pin], pos, pad_value);

	} else {
			
		// Read input
		uint8_t result = 0;
		if ( GPIO_ReadInputDataBit(GPIO_BANK_MAP[pin], GPIO_PIN_MAP[pin]) == Bit_SET ) {
			result = 1;
		}
		// Add mode / pin info
		result |= pin << 1;
		result |= mode << 5;
		// Output the value
		*(outputPosition-1) = result;

	}

	return 0;
}

// Timing decrement callback
void TimingDelay_Decrement(void) {
	if (LEDDelay == 0) {
		LEDDelay = 1000;
		//GPIO_SetBits(GPIOC, GPIO_Pin_10);
	}
	if (LEDDelay == 500) {
		GPIO_ResetBits(GPIOC, GPIO_Pin_10);
	}
	LEDDelay--;
  if (TimingDelay != 0) { 
    TimingDelay--;
  }
  if (I2CTimingDelay != 0) { 
    I2CTimingDelay--;
  }
	if (ATSHATimingDelay != 0) {
		ATSHATimingDelay--;
	}
}

// Network configuration
void Network_Config(void) {
    wiz_NetInfo gWIZNETINFO;

    uint8_t mac_addr[6] = { 0x00, 0x08, 0xDC, 0x01, 0x02, 0x03 };
    uint8_t ip_addr[4] = { 192, 168, 1, 77 };
    uint8_t gw_addr[4] = { 192, 168, 1, 1 };
    uint8_t sub_addr[4] = { 255, 255, 255, 0 };
    //uint8_t dns_addr[4] = { 8, 8, 8, 8 };
		
		memset(gWIZNETINFO.dns, 0, 4);
		gWIZNETINFO.dhcp = 0;
		
		// CRC check
		// Last two bytes are CRC
		uint16_t flashCRC = calculateCRC16((uint8_t *)DAT0_START_ADDR, 64, 0xFFFF);
		uint16_t storedCRC = (((uint16_t)*((uint8_t *)DAT0_START_ADDR+64)) << 8) | ((uint16_t)*((uint8_t *)DAT0_START_ADDR+65));
		
		// If PC15 is pulled high and the flash CRC is good, use the flash data
		if ( (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15) == Bit_SET) && (flashCRC == storedCRC) ) {

			// Flash IP & MAC
			memcpy(gWIZNETINFO.mac, (uint8_t *)DAT0_START_ADDR, 6);
			memcpy(gWIZNETINFO.ip, (uint8_t *)DAT0_START_ADDR+6, 4);
			memcpy(gWIZNETINFO.gw, (uint8_t *)DAT0_START_ADDR+10, 4);
			memcpy(gWIZNETINFO.sn, (uint8_t *)DAT0_START_ADDR+14, 4);
			//memcpy(gWIZNETINFO.dns, (uint8_t *)DAT0_START_ADDR+18, 4);
			//gWIZNETINFO.dhcp = *((uint8_t *)DAT0_START_ADDR+22);
			
		} else {
			// Otherwise use the defaults (192.168.1.77 and 0x0008DC010203)
			
			// Default IP and MAC
			memcpy(gWIZNETINFO.mac, mac_addr, 6);
			memcpy(gWIZNETINFO.ip, ip_addr, 4);
			memcpy(gWIZNETINFO.gw, gw_addr, 4);
			memcpy(gWIZNETINFO.sn, sub_addr, 4);
			//memcpy(gWIZNETINFO.dns, dns_addr, 4);
			//gWIZNETINFO.dhcp = 0;
			
		}

		// Tell the wizchip the MAC
		WIZCHIP_WRITE(WZTOE_SHAR+3, gWIZNETINFO.mac[0]);
		WIZCHIP_WRITE(WZTOE_SHAR+2, gWIZNETINFO.mac[1]);
		WIZCHIP_WRITE(WZTOE_SHAR+1, gWIZNETINFO.mac[2]);
		WIZCHIP_WRITE(WZTOE_SHAR+0, gWIZNETINFO.mac[3]);
		WIZCHIP_WRITE(WZTOE_SHAR+7, gWIZNETINFO.mac[4]);
		WIZCHIP_WRITE(WZTOE_SHAR+6, gWIZNETINFO.mac[5]); 

		// Tell the wizchip the gateway
		WIZCHIP_WRITE((WZTOE_GAR+3), gWIZNETINFO.gw[0]);
		WIZCHIP_WRITE((WZTOE_GAR+2), gWIZNETINFO.gw[1]);
		WIZCHIP_WRITE((WZTOE_GAR+1), gWIZNETINFO.gw[2]);
		WIZCHIP_WRITE((WZTOE_GAR  ), gWIZNETINFO.gw[3]); 
		
		// Tell the wizchip the subnet
    WIZCHIP_WRITE((WZTOE_SUBR+3), gWIZNETINFO.sn[0]);
		WIZCHIP_WRITE((WZTOE_SUBR+2), gWIZNETINFO.sn[1]);
		WIZCHIP_WRITE((WZTOE_SUBR+1), gWIZNETINFO.sn[2]);
		WIZCHIP_WRITE((WZTOE_SUBR  ), gWIZNETINFO.sn[3]);
		
		// Tell the wizchip the ip
    WIZCHIP_WRITE((WZTOE_SIPR+3), gWIZNETINFO.ip[0]);
		WIZCHIP_WRITE((WZTOE_SIPR+2), gWIZNETINFO.ip[1]);
		WIZCHIP_WRITE((WZTOE_SIPR+1), gWIZNETINFO.ip[2]);
		WIZCHIP_WRITE((WZTOE_SIPR  ), gWIZNETINFO.ip[3]);
		
		// NOTE: DHCP / DNS is unused
		//   _DNS_[0] = gWIZNETINFO.dns[0];
		//   _DNS_[1] = pnetinfo->dns[1];
		//   _DNS_[2] = pnetinfo->dns[2];
		//   _DNS_[3] = pnetinfo->dns[3];
		//   _DHCP_   = pnetinfo->dhcp;
}

// Initialize GPIO
void GPIOInit() {
		GPIO_InitTypeDef GPIO_InitDef;
	
	  // Default init settings for outputs
    GPIO_InitDef.GPIO_Direction = GPIO_Direction_OUT; // Set to output
	  GPIO_InitDef.GPIO_Pad = GPIO_PuPd_NOPULL; // No pullup or pulldown
    GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

	  // GPIO UART TX/RX LEDs init
    //GPIO_InitDef.GPIO_Pin = GPIO_Pin_10;
    //GPIO_Init(GPIOC, &GPIO_InitDef);
		//GPIO_InitDef.GPIO_Pin = GPIO_Pin_11;
    //GPIO_Init(GPIOC, &GPIO_InitDef);

		// Set LEDs off to start (inverted)
		//GPIO_SetBits(GPIOC, GPIO_Pin_10);
		//GPIO_SetBits(GPIOC, GPIO_Pin_11);
	
	  // Default network settings pin (PC15)
	  GPIO_InitDef.GPIO_Direction = GPIO_Direction_IN; // Set to input
	  GPIO_InitDef.GPIO_Pad = GPIO_PuPd_UP; // Pullup
    GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

    GPIO_InitDef.GPIO_Pin = GPIO_Pin_15;
    GPIO_Init(GPIOC, &GPIO_InitDef);
		
	  // I2C header

	  // Default pullup, input
	  GPIO_InitDef.GPIO_Direction = GPIO_Direction_IN; // Set to input
	  GPIO_InitDef.GPIO_Pad = GPIO_PuPd_UP; // Pullup
    GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

		for (uint8_t i = 0; i < NUM_I2C_CHANNELS; ++i) {
			GPIO_InitDef.GPIO_Pin = I2C_SCL_PIN_MAP[i];
			GPIO_Init(I2C_SCL_BANK_MAP[i], &GPIO_InitDef);
			GPIO_ResetBits(I2C_SCL_BANK_MAP[i], I2C_SCL_PIN_MAP[i]);
			GPIO_InitDef.GPIO_Pin = I2C_SDA_PIN_MAP[i];
			GPIO_Init(I2C_SDA_BANK_MAP[i], &GPIO_InitDef);
			GPIO_ResetBits(I2C_SDA_BANK_MAP[i], I2C_SDA_PIN_MAP[i]);
		}
		
	  // GPIO pins
	  // Default no pullup, input
	  GPIO_InitDef.GPIO_Direction = GPIO_Direction_IN; // Set to input
	  GPIO_InitDef.GPIO_Pad = GPIO_PuPd_NOPULL; // Pullup
    GPIO_InitDef.GPIO_AF = PAD_AF1; // GPIO AF

		for (uint8_t i = 0; i < NUM_GPIO_CHANNELS; ++i) {
			GPIO_InitDef.GPIO_Pin = GPIO_PIN_MAP[i];
			GPIO_Init(GPIO_BANK_MAP[i], &GPIO_InitDef);
			GPIO_ResetBits(GPIO_BANK_MAP[i], GPIO_PIN_MAP[i]);
		}
		
		// SSP1 AF3
		// PB_00 == GPIO
		*(volatile uint32_t *)(0x41002040) = 0x00000001;
		// PB_01 == GPIO
		*(volatile uint32_t *)(0x41002044) = 0x00000001;
		// PB_02 == GPIO
		*(volatile uint32_t *)(0x41002048) = 0x00000001;
		// PB_03 == GPIO
		*(volatile uint32_t *)(0x4100204C) = 0x00000001;
	  // PA_11 == SSEL1
		*(volatile uint32_t *)(0x4100202C) = 0x00000002;
		// PA_12 == SCLK1
		*(volatile uint32_t *)(0x41002030) = 0x00000002;
		// PA_13 == MISO1
		*(volatile uint32_t *)(0x41002034) = 0x00000002;
		// PA_14 == MOSI1
		*(volatile uint32_t *)(0x41002038) = 0x00000002;

}
