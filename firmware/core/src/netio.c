// NOTE: Modified from examples

/*******************************************************************************************************************************************************
 * Copyright �� 2016 <WIZnet Co.,Ltd.> 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the ��Software��), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED ��AS IS��, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*********************************************************************************************************************************************************/
#include <stdio.h>
#include "netio.h"
#include "wizchip_conf.h"

static uint8_t inputBuffer[DATA_BUF_SIZE];
static uint8_t *inputPosition, *inputEnd;

/*
 * TCPIP loopback server
 * Opens and Initializes socket if closed
 * Only performs 1 action per call, needs to be called multiple times if socket needs initialization
 */
int32_t loopback_tcps(uint8_t sn, uint8_t* buf, uint16_t port)
{
	int32_t ret;
	uint16_t size = 0, sentsize=0;
	char status[80];
	int sstat;							/* status string because serial port is used for debug */

	//uint8_t destip[4];
  //uint16_t destport;

	sstat = getSn_SR(sn);		/* put status in temp reg to facilitate debug */
	switch(sstat)
		{
		/*
		 * socket established, read data and echo back to client
		 */
		case SOCK_ESTABLISHED :
			if(getSn_IR(sn) & Sn_IR_CON)			/* client connection interrupt */
				{
				setSn_IR(sn,Sn_IR_CON);					/* actually sets inetrrupt clear reg, clearing client connect interrupt */
				}
			if((size = getSn_RX_RSR(sn)) > 0) /* received byte count */
				{
				if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE;	/* can't read more than buffer holds */
				ret = recv(sn, buf, size);			/* copy data into local buffer */

				if(ret <= 0) return ret;      	/* return busy status (0) if no data yet */
				sentsize = 0;

				while(size != sentsize)				/* reflect data back to sender */
					{
					ret = send(sn, buf+sentsize, size-sentsize);	/* transmit data, will return 0 if operation in progress */
					if(ret < 0)									/* if error exit */
						{
						close(sn);
						return ret;
						}
					sentsize += ret; 						/* not a problem if still in progress since ret will be 0 */
					}
				}
			break;
		/*
		 * waiting for socket to close, issue disconnect 
		 */
		case SOCK_CLOSE_WAIT :
			if((ret = disconnect(sn)) != SOCK_OK) return ret;
      break;
		/*
		 * initialize socket, issue listen command
		 */
		case SOCK_INIT :
			if( (ret = listen(sn)) != SOCK_OK) return ret;		/* enter listen state - TCP server waiting for connection */
				break;
		/*
		 * socket closed, issue open command
		 */
		case SOCK_CLOSED:
		  // Open socket, no delayed ack
			if((ret = socket(sn, Sn_MR_TCP, port, Sn_MR_ND)) != sn) return ret;
		  // Set don't fragment
		  *(volatile uint32_t *)(0x4601010C) = 0x00000000;
			// Initialise the offsets
			inputPosition = inputBuffer;
			inputEnd = inputBuffer;
			break;
		/*
		 * default is to do nothing, usually the socket is in listen mode here
		 */
		default:
			break;
		}
	return 1;
}
/*
 * TCPIP receive packet
 * Opens and Initializes socket if closed
 * Only performs 1 action per call, needs to be called multiple times if socket needs initialization
 */
int32_t rpacket_tcps(uint8_t sn, uint8_t* buf, uint16_t maxSize, uint16_t port)
{
	int32_t ret;
	uint16_t size = 0;
	char status[80];
	int sstat;							/* status string because serial port is used for debug */

	sstat = getSn_SR(sn);		/* put status in temp reg to facilitate debug */
	switch(sstat)
		{
		/*
		 * socket established, read data
		 */
		case SOCK_ESTABLISHED :
			if(getSn_IR(sn) & Sn_IR_CON)			/* client connection interrupt */
				{
				setSn_IR(sn,Sn_IR_CON);					/* actually sets inetrrupt clear reg, clearing client connect interrupt */
				}
			if((size = getSn_RX_RSR(sn)) > 0) /* received byte count */
				{
				if(size > maxSize) size = maxSize;	/* can't read more than buffer holds */
				ret = recv(sn, buf, size);			/* copy data into local buffer */
				if(ret <= 0) return ret;      	/* return busy status (0) if no data yet */
				return size;
				}
			break;
		/*
		 * waiting for socket to close, issue disconnect 
		 */
		case SOCK_CLOSE_WAIT :
			if((ret = disconnect(sn)) != SOCK_OK) return ret;
			break;
		/*
		 * initialize socket, issue listen command
		 */
		case SOCK_INIT :
			if( (ret = listen(sn)) != SOCK_OK) return ret;		/* enter listen state - TCP server waiting for connection */
				break;
		/*
		 * socket closed, issue open command
		 */
		case SOCK_CLOSED:
			// No delay in ACK
		  if((ret = socket(sn, Sn_MR_TCP, port, Sn_MR_ND)) != sn) return ret;
			// Set don't fragment
		  *(volatile uint32_t *)(0x4601010C) = 0x00000000;
			break;
		/*
		 * default is to do nothing, usually the socket is in listen mode with no client connected here
		 */
		default:
			break;
		}
	return 0;
}
/*
 * TCPIP send packet to client
 * assumes that client is connected and socket is established
 */
int32_t xpacket_tcps(uint8_t sn, uint8_t* buf, uint16_t size, uint16_t port)
{
	int32_t ret;
	uint16_t sentsize=0;
	char status[80];
	int sstat;							/* status string because serial port is used for debug */

	uint8_t destip[4];
  uint16_t destport;

	sstat = getSn_SR(sn);		/* put status in temp reg to facilitate debug */
	switch(sstat)
		{
		/*
		 * socket established, echo back to client
		 */
		case SOCK_ESTABLISHED :
				sentsize = 0;
				while(size != sentsize)				/* reflect data back to sender */
					{
					ret = send(sn, buf+sentsize, size-sentsize);	/* transmit data, will return 0 if operation in progress */
					if(ret < 0)									/* if error exit */
						{
						close(sn);
						return ret;
						}
					sentsize += ret; 						/* not a problem if still in progress since ret will be 0 */
					}
			break;
		/*
		 * waiting for socket to close, issue disconnect 
		 */
		case SOCK_CLOSE_WAIT :
			if((ret = disconnect(sn)) != SOCK_OK) return ret;
			break;
		/*
		 * default is to do nothing, either connection not established or no data to send
		 */
		default:
			break;
		}
	return 1;
}

